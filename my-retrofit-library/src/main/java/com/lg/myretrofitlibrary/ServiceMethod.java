package com.lg.myretrofitlibrary;

import android.support.annotation.Nullable;

import com.lg.myretrofitlibrary.converter.Converter;
import com.lg.myretrofitlibrary.http.Field;
import com.lg.myretrofitlibrary.http.GET;
import com.lg.myretrofitlibrary.http.POST;
import com.lg.myretrofitlibrary.http.Query;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okhttp3.Call;

class ServiceMethod<R,T> {
    // Upper and lower characters, digits, underscores, and hyphens, starting with a character.
    static final String PARAM = "[a-zA-Z][a-zA-Z0-9_-]*";
    static final Pattern PARAM_URL_REGEX = Pattern.compile("\\{(" + PARAM + ")\\}");
    static final Pattern PARAM_NAME_REGEX = Pattern.compile(PARAM);

    private final Call.Factory callFactory;

    private final HttpUrl baseUrl;
    private final String httpMethod;
    private final String relativeUrl;
    private final boolean hasBody;
    private final ParameterHandler[] parameterHandlers;

    ServiceMethod(Builder builder) {
        this.callFactory = builder.retrofit.callFactory();
        this.baseUrl = builder.retrofit.baseUrl();
        this.httpMethod = builder.httpMethod;
        this.relativeUrl = builder.relativeUrl;
        this.parameterHandlers = builder.parameterHandlers;
        this.hasBody = builder.hasBody;
    }




    // 发起请求
    okhttp3.Call toCall(@Nullable Object... args) {
        // 实例化RequestBuilder对象，拼接完整请求url（包含参数名和参数值）
        RequestBuilder requestBuilder = new RequestBuilder(httpMethod, baseUrl, relativeUrl, hasBody);

        // 参考源码
        ParameterHandler[] handlers = parameterHandlers;

        int argumentCount = args != null ? args.length : 0;
        // Proxy方法的参数个数是否等于参数的数组（手动添加）的长度，此处理解为校验
        if (argumentCount != handlers.length) {
            throw new IllegalArgumentException("Argument count (" + argumentCount
                    + ") doesn't match expected count (" + handlers.length + ")");
        }

        // 循环拼接每个参数名和参数值
        for (int i = 0; i < argumentCount; i++) {
            // 方法参数的数组中每个对象已经调用了对应实现方法
            handlers[i].apply(requestBuilder, args[i].toString());
        }

        // 参考源码，创建请求
        return callFactory.newCall(requestBuilder.build());
    }

    static final class Builder{


        // OkHttpClick封装构建
        final Retrofit retrofit;
        // 带注解的方法
        final Method method;
        // 方法的所有注解（方法可能有多个注解）
        final Annotation[] methodAnnotations;
        // 方法参数的所有注解（一个方法有多个参数，一个参数有多个注解）
        final Annotation[][] parameterAnnotationsArray;
        // 方法的请求方式（"GET"、"POST"）
        private String httpMethod;
        // 方法的注解的值（"/ip/ipNew"）
        private String relativeUrl;
        // 方法参数的数组（每个对象包含：参数注解值、参数值）
        private ParameterHandler[] parameterHandlers;
        // 是否有请求体（GET方式没有）
        private boolean hasBody;



        Builder(Retrofit retrofit, Method method) {
            this.retrofit = retrofit;
            this.method = method;
            // 获取方法的所有注解
            this.methodAnnotations = method.getAnnotations();
            // 获取方法参数的所有注解
            this.parameterAnnotationsArray = method.getParameterAnnotations();
        }

        ServiceMethod build() {
            // 参考源码，获取方法的请求方式、方法的注解的值
            // 遍历方法的每个注解（我们只需要GET或者POST注解）
            for (Annotation annotation : methodAnnotations) {
                parseMethodAnnotation(annotation);
            }

            // 定义方法参数的数组长度
            int parameterCount = parameterAnnotationsArray.length;
            // 初始化方法参数的数组
            parameterHandlers = new ParameterHandler[parameterCount];
            // 遍历方法的参数（我们只需要Query或者Field注解）
            for (int i = 0; i < parameterCount; i++) {
                // 获取每个参数的所有注解
                Annotation[] parameterAnnotations = parameterAnnotationsArray[i];
                // 如果该参数没有任何注解抛出异常
                if (parameterAnnotations == null) {
                    throw new IllegalArgumentException("No Retrofit annotation found." + " (parameter #" + (i + 1) + ")");
                }

                // 参考源码，获取参数的注解值、参数值
                parameterHandlers[i] = parseParameter(i, parameterAnnotations);
            }
            return new ServiceMethod(this);
        }

        // 解析方法的注解，可能是GET或者POST
        private void parseMethodAnnotation(Annotation annotation) {
            // 参考源码
            if (annotation instanceof GET) {
                // 注意：GET方式没有请求体RequestBody
                parseHttpMethodAndPath("GET", ((GET) annotation).value(), false);
            } else if (annotation instanceof POST) {
                parseHttpMethodAndPath("POST", ((POST) annotation).value(), true);
            }
        }

        // 通过方法的注解，获取方法的请求方式、方法的注解的值
        private void parseHttpMethodAndPath(String httpMethod, String value, boolean hasBody) {
            // 参考源码
            // 方法的请求方式（"GET"、"POST"）
            this.httpMethod = httpMethod;
            // 方法的注解的值（"/ip/ipNew"）
            this.relativeUrl = value;
            // 是否有请求体
            this.hasBody = hasBody;
        }

        // 解析参数的所有注解（嵌套循环）
        private ParameterHandler parseParameter(int i, Annotation[] annotations) {
            // 参考源码
            ParameterHandler result = null;
            // 遍历参数的注解，如：(@Query("ip") @Field("ip") String ip)
            for (Annotation annotation : annotations) {
                // 注解可能是Query或者Field
                ParameterHandler annotationAction = parseParameterAnnotation(annotation);
                // 找不到继续找，可不写（增强代码健壮）
                if (annotationAction == null) {
                    continue;
                }
                // 赋值
                result = annotationAction;
            }
            // 如果该参数没有任何注解抛出异常
            if (result == null) {
                throw new IllegalArgumentException("No Retrofit annotation found." + " (parameter #" + (i + 1) + ")");
            }

            return result;
        }

        // 解析参数的注解，可能是Query或者Field
        private ParameterHandler parseParameterAnnotation(Annotation annotation) {
            // 参考源码
            if (annotation instanceof Query) {
                Query query = (Query) annotation;
                String name = query.value();
                // 注意：传过去的参数是注解的值，并非参数值。参数值由Proxy方法传入
                return new ParameterHandler.Query(name);
            } else if (annotation instanceof Field) {
                Field field = (Field) annotation;
                String name = field.value();
                // 注意：传过去的参数是注解的值，并非参数值。参数值由Proxy方法传入
                return new ParameterHandler.Field(name);
            }
            return null;
        }
    }


}

package com.lg.myretrofitlibrary;

import android.support.annotation.Nullable;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import okio.Timeout;

/**
 * 自定义OkHttpCall，和源码一样
 */
public class OkHttpCall implements Call {

    private final ServiceMethod serviceMethod;
    private final Object[] args;
    private final Call rawCall;

    /**
     * 实例化okhttp3.Call rawCall对象
     *
     * @param serviceMethod 调用toCall(args)方法
     * @param args 方法参数
     */
    OkHttpCall(ServiceMethod serviceMethod, @Nullable Object[] args) {
        this.serviceMethod = serviceMethod;
        this.args = args;
        this.rawCall = serviceMethod.toCall(args);
    }

    // 本身自带实现，直接调用接口，自动寻找实现方法
    @Override
    public Request request() {
        return rawCall.request();
    }

    @Override
    public Response execute() throws IOException {
        return rawCall.execute();
    }

    @Override
    public void enqueue(Callback responseCallback) {
        rawCall.enqueue(responseCallback);
    }

    @Override
    public void cancel() {
        rawCall.cancel();
    }

    @Override
    public boolean isExecuted() {
        return rawCall.isExecuted();
    }

    @Override
    public boolean isCanceled() {
        return rawCall.isCanceled();
    }

    @Override
    public Timeout timeout() {
        return rawCall.timeout();
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public Call clone() {
        return new OkHttpCall(serviceMethod, args);
    }
}

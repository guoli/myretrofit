package com.lg.myretrofit;

import com.lg.myretrofitlibrary.http.GET;
import com.lg.myretrofitlibrary.http.Query;

import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import okhttp3.Call;
import okhttp3.Response;


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MyProxyUnitTest {
    String baseUrl = "http://op.juhe.cn/";
    String from="CNY";
    String to = "USD";
    String key="bcbb7c78be50cc903e30632ed8c439d3";

    interface RestAPI{

        //Call<ResponseBody> get(@Query("from") String from,@Query("to") String to,  @Query("key") String key);
        @GET("onebox/exchange/currency")
        Call get(@Query("from") String from, @Query("to") String to, @Query("key") String key);
    }


    @Test
    public void myProxyTest() {

        //Retrofit retrofit = new Retrofit.Builder()
        //                .baseUrl(baseUrl)
        //                .build();



        //2.动态代理模式
        RestAPI restAPI = (RestAPI) Proxy.newProxyInstance(RestAPI.class.getClassLoader(),new Class[]{RestAPI.class},new InvocationHandler(){

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                // 获取方法名称
                System.out.println("获取方法名称 >>> " + method.getName());
                // 获取方法的注解
                GET get = method.getAnnotation(GET.class);
                // 获取方法的注解值
                System.out.println("获取方法的注解值 >>> " + get.value());
                // 获取方法的参数的注解
                Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                for (Annotation[] annotation : parameterAnnotations) {
                    System.out.println("获取方法的参数的注解 >>> " + Arrays.toString(annotation));
                }
                // 获取方法的参数值
                System.out.println("获取方法的参数值 >>> " + Arrays.toString(args));


                return null;
            }
        });

        restAPI.get(from,to,key);



        }

    }


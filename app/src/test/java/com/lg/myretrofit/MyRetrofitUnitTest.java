package com.lg.myretrofit;

import com.lg.myretrofit.bean.Repo;
import com.lg.myretrofitlibrary.Call;
import com.lg.myretrofitlibrary.Response;
import com.lg.myretrofitlibrary.Retrofit;
import com.lg.myretrofitlibrary.adapter.CallAdapter;
import com.lg.myretrofitlibrary.converter.Converter;
import com.lg.myretrofitlibrary.converter.gson.GsonConverterFactory;
import com.lg.myretrofitlibrary.http.GET;
import com.lg.myretrofitlibrary.http.Query;

import org.junit.Test;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.ResponseBody;


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MyRetrofitUnitTest {
    String baseUrl = "http://op.juhe.cn/";
    String from="CNY";
    String to = "USD";
    String key="bcbb7c78be50cc903e30632ed8c439d3";

    interface RestAPI{

        //Call<ResponseBody> get(@Query("from") String from,@Query("to") String to,  @Query("key") String key);
        @GET("onebox/exchange/currency")
        Call<Repo> get(@Query("from") String from, @Query("to") String to, @Query("key") String key);
    }



    @Test
    public void MyRetrofit() {

        //Retrofit retrofit = new Retrofit.Builder()
        //                .baseUrl(baseUrl)
        //                .build();

        //1.责任链模式
        Retrofit myRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                //.addConverterFactory(GsonConverterFactory.create())
                .build();
        //2.动态代理模式
        RestAPI host = myRetrofit.create(RestAPI.class);

        // Retrofit GET同步请求
        {
            Call<Repo> call = host.get(from,to, key);
            try {

                //Call<List<Repo>> call = github.listRepos("square");
                //List<Repo> repos = call.execute().body();
                //List<Repo> repos = call.execute().body();
                Response<Repo> repos  = call.execute();
                if (repos != null && repos.toString() != null) {
                    System.out.println("MyRetrofit GET同步请求 >>> " + repos.body().getResult().getCountry());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }


}
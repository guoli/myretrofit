package com.lg.myretrofit;

import com.lg.myretrofit.bean.Repo;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class RetrofitTest {
    //请求地址：http://op.juhe.cn/onebox/exchange/currency
    //请求参数：from=CNY&to=USD&key=bcbb7c78be50cc903e30632ed8c439d3
    //ip=112.112.11.11&key=224b189c4cdd14e9b112adcd77c51d2a

    //String baseUrl = "http://op.juhe.cn/";
    String baseUrl = "http://apis.juhe.cn/";
    String from="CNY";
    String to = "USD";
   // String key="bcbb7c78be50cc903e30632ed8c439d3";
    String ip ="112.112.11.11";
    String key="224b189c4cdd14e9b112adcd77c51d2a";
    interface RESTAPI{
        //@GET("onebox/exchange/currency")
        @GET("ip/ipNew")
        Call <Repo>  get(@Query("ip") String ip, @Query("key") String key);
        //Call<ResponseBody>  get(@Query("from") String from, @Query("to") String to, @Query("key") String key);

        @POST("onebox/exchange/currency")
        @FormUrlEncoded
        Call<Repo> post(@Field("from") String from,@Field("to") String to, @Field("key") String key);
    }

    @Test
    public void retrofit(){




        //Get
        // ----------------------------------- Retrofit2 -----------------------------------

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RESTAPI host = retrofit.create(RESTAPI.class);





        // Retrofit GET同步请求
        {
            Call<Repo> call = host.get(ip, key);
            try {
               Response<Repo> response  = call.execute();
                     if (response != null && response.body() != null) {
                            System.out.println("Retrofit GET同步请求 >>> " + response.body().getResult().getCity());
                        }
//                call.enqueue(new Callback<Repo>() {
//                    @Override
//                    public void onResponse(Call<Repo> call, Response<Repo> response) {
//                        if (response != null && response.body() != null) {
//                            System.out.println("Retrofit GET同步请求 >>> " + response.body().getResult().getCity());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<Repo> call, Throwable t) {
//                        if (t != null ) {
//                            System.out.println("Retrofit Exception >>> " + t.getMessage());
//                        }
//                    }
//                });

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Retrofit Exception >>> " + e.getMessage());

            }

        }


//        //post
//        {
//            Call<Repo> call = host.post(from,to,key);
//            try {
//                Response<Repo> response  = call.execute();
//                if (response != null && response.body() != null) {
//                    System.out.println("Retrofit Post同步请求 >>> " + response.toString());
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

    }
}
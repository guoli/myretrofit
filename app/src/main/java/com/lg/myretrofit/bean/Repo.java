/**
 * Copyright 2019 bejson.com
 */
package com.lg.myretrofit.bean;

/**
 * Auto-generated: 2019-08-19 8:6:0
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Repo {

    private String resultcode;
    private String reason;
    private Result result;
    private int error_code;
    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }
    public String getResultcode() {
        return resultcode;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getReason() {
        return reason;
    }

    public void setResult(Result result) {
        this.result = result;
    }
    public Result getResult() {
        return result;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }
    public int getError_code() {
        return error_code;
    }



    public class Result {

        private String Country;
        private String Province;
        private String City;
        private String Isp;
        public void setCountry(String Country) {
            this.Country = Country;
        }
        public String getCountry() {
            return Country;
        }

        public void setProvince(String Province) {
            this.Province = Province;
        }
        public String getProvince() {
            return Province;
        }

        public void setCity(String City) {
            this.City = City;
        }
        public String getCity() {
            return City;
        }

        public void setIsp(String Isp) {
            this.Isp = Isp;
        }
        public String getIsp() {
            return Isp;
        }

    }


}